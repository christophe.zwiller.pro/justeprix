#include <iostream>
using namespace std;

const auto BORNE_MIN{0};
const auto BORNE_MAX{10'000};
const auto BORNE_MAX_FACILE{1'000};

enum class Choix_menu {

    JOUER = 'a',
    JOUER_FACILE = 'b',
    JOUER_3_PARTIES = 'c',
    QUITTER = 'q',
    INCORRECT
};

Choix_menu Demander_choix_menu(){

    char saisie_choix;
    cin >> saisie_choix;
    if(saisie_choix == static_cast<char>(Choix_menu::JOUER) || saisie_choix == static_cast<char>(Choix_menu::JOUER_FACILE) ||
    saisie_choix == static_cast<char>(Choix_menu::JOUER_3_PARTIES) || saisie_choix == static_cast<char>(Choix_menu::QUITTER)){
        return static_cast<Choix_menu>(saisie_choix);
    } else {
        return Choix_menu::INCORRECT;
    }

}

void afficher_menu(){

    cout << "---------- Menu du jeu ----------" << endl;
    cout << static_cast<char>(Choix_menu::JOUER) << " : jouer" << endl;
    cout << static_cast<char>(Choix_menu::JOUER_FACILE) << " : jouer (facile)" << endl;
    cout << static_cast<char>(Choix_menu::JOUER_3_PARTIES) << " : jouer 3 parties" << endl;
    cout << static_cast<char>(Choix_menu::QUITTER) << " : quitter" << endl;
    cout << "---------------------------------"    << endl << endl;

}

void demander_proposition(int &proposition){
    cin >> proposition;
}

void jouer_partie(int le_juste_prix, int max){
    auto proposition{0};
    auto nombre_tentatives{0};

    cout << "C'est parti !" << endl << endl;

    do{

        cout << "=> Proposition ? : ";
        demander_proposition(proposition);
        nombre_tentatives++;

        if(proposition >= BORNE_MIN && proposition <= max){

            if(proposition == le_juste_prix){
                    cout << "Bravo !" << endl;
                }
                else if(proposition > le_juste_prix){
                    cout << "C'est moins !" << endl << endl;
                }
                else {
                    cout << "C'est plus !" << endl << endl;
                }
        }

    } while(proposition != le_juste_prix);

    cout << "Partie terminee en " << nombre_tentatives << " tentatives" << endl << endl;

}

void jouer_3_parties(){

    for(auto valeur_prix : {1, 2, 3}){    //range for base
        jouer_partie(valeur_prix, BORNE_MAX);
    }
}

int main(){

    cout << "=> Bienvenue au jeux du juste prix !" << endl << endl;


    bool continuer{true};

    while(continuer != false){

        afficher_menu();
        auto choix = Demander_choix_menu();

        switch(choix){
            case Choix_menu::JOUER:
                jouer_partie(3000, BORNE_MAX);
                break;

            case Choix_menu::JOUER_FACILE:
                jouer_partie(250, BORNE_MAX_FACILE);
                break;

            case Choix_menu::JOUER_3_PARTIES:
                jouer_3_parties();
                break;

            case Choix_menu::QUITTER:
                cout << "Au-revoir !" << endl << endl;
                continuer = false;
                break;

            case Choix_menu::INCORRECT:

            default:
                cout << "Erreur" << endl;
                cout << "=> Arrêt du jeu" << endl;
                return EXIT_FAILURE;  //1
        }

    }

    cout << "Fin du jeux" << endl;
}


